const env = require('../../config/environment.json')[process.env.npm_lifecycle_event];

// @todo: investigar medidas de seguridad para evitar ataques
// @todo: https://www.geekboy.ninja/blog/exploiting-misconfigured-cors-cross-origin-resource-sharing/
// @link: https://portswigger.net/research/exploiting-cors-misconfigurations-for-bitcoins-and-bounties
module.exports = function(req, res, next) {
    
    if (env.originUrl.indexOf(req.headers.origin) !== -1) {
        // Website you wish to allow to connect
        res.header('Access-Control-Allow-Origin', req.headers.origin);
         // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');



        // res.setHeader('Access-Control-Request-Headers', 'withCredentials ');
        // res.setHeader('Access-Control-Request-Methods', 'POST');

    }
   
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
};
