class ErrorHandler extends Error {
    constructor(statusCode, message, error) {
        super();
        this.statusCode = statusCode;
        this.message = message;
        if (error) {
            //console.log('GLOBAL ERR -> ', error);
        }
    }
}

const handleError = (err, res, next) => {
    try {
        const { statusCode, message } = err;
        // en caso de enviar un res.status(500) en otro *.js
        // el !res.headersSent evita que se muestre otro error en el server
        console.log('GLOBAL ERR -> ', statusCode);
        console.log('GLOBAL ERR -> ', message);
        res.status(statusCode || 500).json({
            status: false,
            statusCode: statusCode || 500,
            message: message,
            data: {
                internal_message: message,
                user_message: message,
                status_code: statusCode || 500,
            }
        });
        next();
    } catch (error) {
        next(error);
    }
};

module.exports = {
    ErrorHandler,
    handleError
};
