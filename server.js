const express = require('express');
const { handleError } = require('./shared/helpers/error');
const app = express();
const cors = require('./shared/helpers/cors');

const environment = require('./config/environment.json')[process.env.npm_lifecycle_event];
const config = require('./config/environment.json');

const httpServer = require('http').createServer(app);

const router = require('./network/routes');


const PORT = config.global.PORT || 3000;

if (environment.production) process.env.NODE_ENV = 'production';
else process.env.NODE_ENV = 'development';

app.use(express.static('public'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json({ limit: '20mb', extended: true }));
app.use(cors); 
router(app);

app.use((err, req, res, next) => {
    handleError(err, res, next);
});


console.log(`-> Variables de entorno cargadas: ${environment.name}`);
console.log(`-> process.env.NODE_ENV: ${process.env.NODE_ENV}`);

httpServer.listen(PORT, () => {
    console.log(`-> Express ha iniciado correctamente, puerto: ${PORT}`);
    
});
