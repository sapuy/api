const bio = require('./../components/bio/network');

const routes = (server) => {
    server.use('/bio', bio);

}

module.exports = routes;