const express = require('express');
const controller = require('./controller');
const router = express.Router();

/**
 * Get info for any bio at Torre.co
 */
router.get('/:username', (req, res, next) => {
    (async () => {
        try {
            let data=await controller.getBio(req.params);

            res.status(200).json({
                status: true,
                statusCode: 200,
                message: 'OK',
                data:data
                
            }).end();
        } catch (e) { next(e) }
    })()
});

module.exports = router;