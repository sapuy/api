const superagent = require('superagent');
const rfr = require('rfr');
const config = rfr('config/environment.json');
const { ErrorHandler } = rfr('shared/helpers/error');
const D = true;

/**
 * Function to get complete bio from a defined user at Torre.co
 * @param {*} params 
 */
async function getBio(params) {
    try {
        if (D) console.log("BIO - request", params);

        let url = config.global.WS.bio + params.username;
        if (D) console.log("BIO - URL", url);

        let res = await superagent.get(url);
        return res.body

    } catch (err) {
        console.log("BIO - ERROR",err);
        throw new ErrorHandler(err.status, err.response.text, err);
    }
}

module.exports = {
    getBio,

};